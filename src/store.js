import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
      images:[],
      unsplashId:'33dd4f3a997e64e3b7edb7dee9e8670b6f0cd5b38d2cf4a20852d8ef381ad030'
    },
    mutations:{
      Images(state,data){
        state.images = data
      }
    },
    actions: {
       getImages:({commit,state})=>{
          let appId = state.unsplashId
          axios.get('https://api.unsplash.com/search/photos', {
            params:{
              client_id: appId,
              page: '1',
              query: 'office',
              per_page: '120',
              orientation:'landscape',
            }
          })
          .then(function (response) { 
            commit('Images',response.data.results);
          })
          .catch(function (error) {
          console.log(error);
          });
       },
       filterImages:({commit,state},query)=>{
        let appId = state.unsplashId
        axios.get('https://api.unsplash.com/search/photos', {
          params:{
            client_id: appId,
            page: '1',
            query: query,
            per_page: '120',
            orientation:'landscape',
          }
        })
        .then(function (response) { 
          commit('Images',response.data.results);
        })
        .catch(function (error) {
        console.log(error);
        });
       }
    },
    getters: {
      fillImages: state =>{
        let images = []
        state.images.forEach(function(item){
            if(item.description == null) {
              return;
            }else{
              item = {
                id:item.id,
                description:item.description,
                url:item.urls.small,
                name:item.photo_tags[0].title
              }
  
              images.push(item)
            }

        })
        
        return images
      }
    }
})