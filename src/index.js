import Vue from 'vue'
import App from './App.vue'
import store from './store'
import router from './router'
import 'bootstrap/scss/bootstrap.scss'
import 'bootstrap-vue/src/index.scss'

new Vue({
  router,
  store,
  el: '#app',
  render: h => h(App)
})