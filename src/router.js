import Vue from 'vue'
import Router from 'vue-router'
import Detail from '@/Views/Detail.vue'
import Home from '@/Views/Home.vue'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/article/:id',
      name: 'Detail',
      component: Detail
    }
  ]
})